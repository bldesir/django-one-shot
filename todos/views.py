from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos}
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todo}
    return render(request, "todos/todo_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoListForm

    context = {"form": form}

    return render(request, "todos/todo_create.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance=todo)

    context = {"todo_object": todo, "form": form}

    return render(request, "todos/todo_update.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoItemForm()

    context = {"form": form, "todo_item": item}

    return render(request, "todos/todo_item_update.html", context)
